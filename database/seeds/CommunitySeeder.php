<?php

use Illuminate\Database\Seeder;
use App\Community;

class CommunitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default = [
            'sport',
            'music',
            'news',
            'politics',
            'technology',
            'movies',
        ];

        $creator = 2;

        foreach($default as $name)
        {
            Community::firstOrCreate([
                'name' => $name,
            ],[
                'user_id' => $creator,
            ]);
        }
    }
}
