<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default = [
            [
                'title' => 'guest',
                'auth_level' => 1,
            ],
            [
                'title' => 'regular',
                'auth_level' => 2,
            ],
            [
                'title' => 'premium',
                'auth_level' => 3,
            ],
            [
                'title' => 'admin',
                'auth_level' => 4,
            ],
            [
                'title' => 'dev',
                'auth_level' => 99,
            ],
        ];

        foreach($default as $role)
        {
            Role::firstOrCreate([
                'title' => $role['title'],
            ],[
                'auth_level' => $role['auth_level'],
            ]);
        }
    }
}
