<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::firstOrCreate([
            'name' => 'default',
            'email' => 'default@xowder.com'
        ],
            [
                'password' => 'default',
                'role_id' => 1,
                'auth_id' => 1,
                'avatar' => null,
            ]);
    }
}
