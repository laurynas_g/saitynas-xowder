export const POST_REQUEST = 'POST_REQUEST';
export const DELETE_POST = 'DELETE_POST';
export const EDIT_POST = 'EDIT_POST';
export const POST_SUCCESS = 'POST_SUCCESS';
export const POST_ERROR = 'POST_ERROR';
export const NEW_POST = 'NEW_POST';
export const POSTS = 'POSTS';