export const USER_REQUEST = 'USER_REQUEST';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_ERROR = 'USER_ERROR';
export const NEW_COMMUNITY = 'NEW_COMMUNITY';
export const INFO_LOADED = 'INFO_LOADED';