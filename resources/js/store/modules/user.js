import {USER_REQUEST, USER_ERROR, USER_SUCCESS, INFO_LOADED} from '../actions/user'
import {AUTH_LOGOUT} from '../actions/auth'
import apiCall from '../../utils/api'
import Vue from 'vue'
import {COMMUNITIES} from "../actions/community";

const state = {
    status: '',
    profile: (JSON.parse(localStorage.getItem('user'))) || {},
    communities: [],
    loaded: false,
};

const getters = {
    getProfile: state => state.profile || {},
    isProfileLoaded: state => !!state.profile.name,
    isUserInfoLoaded: state => state.loaded,
};

const actions = {
    [USER_REQUEST]: ({commit, dispatch}) => {
        commit(USER_REQUEST);
        dispatch(COMMUNITIES);
        apiCall({url: 'users/me'})
            .then(resp => {
                commit(USER_SUCCESS, resp)
            })
            .catch(err => {
                commit(USER_ERROR);
                dispatch(AUTH_LOGOUT)
            })
    },
};

const mutations = {
    [USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [USER_SUCCESS]: (state, resp) => {
        state.status = 'success';
        Vue.set(state, 'profile', resp);
        localStorage.setItem('user', JSON.stringify(resp));
    },
    [USER_ERROR]: (state) => {
        state.status = 'error'
    },
    [AUTH_LOGOUT]: (state) => {
        state.profile = {}
    },
    [INFO_LOADED]: (state) => {
        state.loaded = true
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
}