import {AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT, REG_REQUEST} from '../actions/auth'
import {USER_REQUEST} from '../actions/user'
import apiCall from '../../utils/api'
import axios from 'axios'

const state = {
    token: localStorage.getItem('user-token') || '',
    status: '',
};

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
};

const actions = {
    [AUTH_REQUEST]: ({commit, dispatch}, user) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST);
            apiCall({url: 'login', data: user, method: 'POST'})
                .then(response => {
                    localStorage.setItem('user-token', response.token);
                    commit(AUTH_SUCCESS, response);
                    dispatch(USER_REQUEST)
                        .then(() => {
                            resolve(response)
                        });
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    localStorage.removeItem('user-token');
                    reject(err)
                })
        })
    },
    [AUTH_LOGOUT]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_LOGOUT);
            localStorage.clear();
            delete axios.defaults.headers.common['Authorization'];
            location.reload();
            resolve();
        })
    },
    [REG_REQUEST]: ({commit, dispatch}, {name, email, password}) => {
        return new Promise((resolve, reject) => {
            commit(REG_REQUEST);
            axios.post('api/register', {name, email, password})
                .then(response => {
                    localStorage.setItem('user-token', response.data.token);
                    commit(AUTH_SUCCESS, response.data);
                    dispatch(USER_REQUEST)
                        .then(() => {
                            resolve(response.data)
                        });
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    reject(err)
                })
        })
    },
};

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = 'success';
        state.token = resp.token;
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error';
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = ''
    },
    [REG_REQUEST]: (state) => {
        state.status = 'loading'
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}