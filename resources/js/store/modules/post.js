import {POST_REQUEST, POST_SUCCESS, POST_ERROR, NEW_POST, POSTS, DELETE_POST, EDIT_POST} from '../actions/post'
import apiCall from '../../utils/api'

const state = {
    status: '',
    posts: [],
};

const getters = {
    getPosts: state => state.posts || [],
};

const actions = {
    [POST_REQUEST]: ({commit, dispatch}, {community, post}) => {
        return new Promise((resolve, reject) => {
            commit(POST_REQUEST);
            apiCall({url: 'communities/' + community + '/posts/' + post})
                .then(response => {
                    commit(POST_SUCCESS, response);
                    resolve(response)
                })
                .catch(err => {
                    commit(POST_ERROR, err);
                    reject(err)
                })
        })
    },
    [NEW_POST]: ({commit, dispatch}, {community, title, editorData}) => {
        return new Promise((resolve, reject) => {
            commit(POST_REQUEST);
            apiCall({url: 'communities/' + community + '/posts', data: {community, title, editorData}, method: 'POST'})
                .then(response => {
                    commit(POST_SUCCESS, response);
                    resolve(response)
                })
                .catch(err => {
                    commit(POST_ERROR, err);
                    reject(err)
                })
        })
    },
    [EDIT_POST]: ({commit, dispatch}, {community, post, title, editorData}) => {
        return new Promise((resolve, reject) => {
            commit(POST_REQUEST);
            apiCall({
                url: 'communities/' + community + '/posts/' + post,
                data: {community, title, editorData},
                method: 'PUT'
            })
                .then(response => {
                    commit(POST_SUCCESS, response);
                    resolve(response)
                })
                .catch(err => {
                    commit(POST_ERROR, err);
                    reject(err)
                })
        })
    },
    [DELETE_POST]: ({commit, dispatch}, {community, post}) => {
        return new Promise((resolve, reject) => {
            commit(POST_REQUEST);
            apiCall({url: 'communities/' + community + '/posts/' + post, method: 'DELETE'})
                .then(response => {
                    commit(POST_SUCCESS, response);
                    resolve(response)
                })
                .catch(err => {
                    commit(POST_ERROR, err);
                    reject(err)
                })
        })
    },
};

const mutations = {
    [POST_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [POST_SUCCESS]: (state, resp) => {
        state.status = 'success';
    },
    [POST_ERROR]: (state) => {
        state.status = 'error';
    },
    [NEW_POST]: (state, resp) => {
        state.token = 'loading'
    },
    [POSTS]: (state, resp) => {
        state.status = 'loading'
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}