import {
    COMMUNITY_REQUEST,
    POSTS_REQUEST,
    COMMUNITY_SUCCESS,
    COMMUNITY_ERROR,
    NEW_COMMUNITY,
    COMMUNITIES,
    SUBSCRIBE,
    UNSUBSCRIBE,
    SUBSCRIPTION_SUCCESS,
    UNSUBSCRIPTION_SUCCESS
} from '../actions/community'
import {INFO_LOADED} from '../actions/user'
import apiCall from '../../utils/api'

const state = {
    status: '',
    subscriptions: [],
    defaultCommunities: [
        {
            name: 'community',
            params: {
                community: 'sport'
            },
        },
        {
            name: 'community',
            params: {
                community: 'news'
            },
        },
        {
            name: 'community',
            params: {
                community: 'music'
            },
        },
        {
            name: 'community',
            params: {
                community: 'politics'
            },
        },
        {
            name: 'community',
            params: {
                community: 'technology'
            },
        },
        {
            name: 'community',
            params: {
                community: 'movies'
            },
        },
    ],
};

const getters = {
    getSubscriptions: state => state.subscriptions || [],
    getDefaultCommunities: state => state.defaultCommunities,
};

const actions = {
    [NEW_COMMUNITY]: ({commit, dispatch}, community) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'communities', data: community, method: 'POST'})
                .then(response => {
                    commit(COMMUNITY_SUCCESS, response);
                    resolve(response);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
    [COMMUNITIES]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'subscriptions'})
                .then(response => {
                    commit(COMMUNITIES, response);
                    commit(INFO_LOADED);
                    resolve(response);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
    [SUBSCRIBE]: ({commit, dispatch}, community) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'subscribe', data: community, method: 'POST'})
                .then(response => {
                    commit(SUBSCRIPTION_SUCCESS, response);
                    resolve(response);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
    [UNSUBSCRIBE]: ({commit, dispatch}, subscription) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'unsubscribe/' + subscription.id, method: 'DELETE'})
                .then(response => {
                    commit(UNSUBSCRIPTION_SUCCESS, subscription);
                    resolve(response);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
    [COMMUNITY_REQUEST]: ({commit, dispatch}, community) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'community/' + community})
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
    [POSTS_REQUEST]: ({commit, dispatch}, community) => {
        return new Promise((resolve, reject) => {
            apiCall({url: 'communities/' + community + '/posts'})
                .then(response => {
                    resolve(response.data);
                })
                .catch(err => {
                    commit(COMMUNITY_ERROR);
                    reject(err);
                })
        })
    },
};

const mutations = {
    [COMMUNITY_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [COMMUNITY_SUCCESS]: (state, resp) => {
        state.status = 'success';
        state.subscriptions.push(resp.data)
    },
    [COMMUNITY_ERROR]: (state) => {
        state.status = 'error'
    },
    [COMMUNITIES]: (state, resp) => {
        state.subscriptions = resp;
    },
    [SUBSCRIPTION_SUCCESS]: (state, resp) => {
        state.status = 'success';
        state.subscriptions.push(resp)
    },
    [UNSUBSCRIPTION_SUCCESS]: (state, resp) => {
        state.status = 'success';
        for (let i = state.subscriptions.length - 1; i >= 0; i--) {
            if (state.subscriptions[i].id === resp.id) {
                state.subscriptions.splice(i, 1);
            }
        }
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
}