import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import auth from './modules/auth'
import community from './modules/community'
import post from './modules/post'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        auth,
        community,
        post,
    },
})