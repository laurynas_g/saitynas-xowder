import Front from '../views/Front'
import Login from '../views/Login'
import Community from '../views/Community'
import Post from '../views/Post'
import CommunityPosts from '../views/CommunityPosts'
import NewPost from '../views/NewPost'
import EditPost from '../views/EditPost'
import Register from '../views/Register'
import Logout from '../views/Logout'
import store from "../store";

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/login')
};

export default [
    {
        path: '',
        component: {
            template: '<router-view/>',
        },
        children: [
            {
                path: '',
                name: 'front',
                component: Front,
            },
            {
                path: 'x/:community',
                component: Community,
                children: [
                    {
                        path: '',
                        name: 'community',
                        component: CommunityPosts,
                    },
                    {
                        path: 'new',
                        name: 'new-post',
                        component: NewPost,
                        beforeEnter: ifAuthenticated,
                    },
                    {
                        path: ':post',
                        name: 'post',
                        component: Post,
                    },
                    {
                        path: ':post/edit',
                        name: 'edit-post',
                        component: EditPost,
                        beforeEnter: ifAuthenticated,
                    },
                    {
                        path: '*',
                        redirect: 'posts',
                    },
                ],
            },
            {
                path: 'login',
                name: 'login',
                component: Login,
                beforeEnter: ifNotAuthenticated,
            },
            {
                path: 'register',
                name: 'register',
                component: Register,
                beforeEnter: ifNotAuthenticated,
            },
            {
                path: 'logout',
                name: 'logout',
                component: Logout,
                beforeEnter: ifAuthenticated,
            },
            {
                path: '*',
                redirect: 'front',
            },
        ],
    },
];