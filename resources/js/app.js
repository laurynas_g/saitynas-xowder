window.Vue = require('vue');
window.axios = require('axios');

require('./componentList');

import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router'
import router from './router';
import store from './store'
import App from './components/App';
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(CKEditor);

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store,
});