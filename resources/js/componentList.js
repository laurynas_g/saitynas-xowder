import Vue from 'vue';

Vue.component('page-template', require('./templates/PageTemplate'));

Vue.component('app', require('./components/App'));
Vue.component('x-header', require('./components/Header'));
Vue.component('x-footer', require('./components/Footer'));
Vue.component('x-btn', require('./components/XButton'));
Vue.component('x-logo', require('./components/Logo'));
Vue.component('x-logo-min', require('./components/LogoMin'));
Vue.component('x-burger-menu', require('./components/XBurgerMenu'));
Vue.component('x-dashboard', require('./components/Dashboard'));
Vue.component('x-modal', require('./components/XModal'));
Vue.component('side-bar', require('./components/SideBar'));
Vue.component('community-form', require('./components/CommunityForm'));
Vue.component('subscription-button', require('./components/SubscriptionButton'));
Vue.component('new-post-button', require('./components/NewPostButton'));