const apiCall = ({url, method, ...args}) => new Promise((resolve, reject) => {
    try {
        axios({
            method: method || 'GET',
            url: '/api/' + url,
            data: args,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('user-token') || '',
            }
        })
            .then(function (response) {
                resolve(response.data)
            })
            .catch(function (err) {
                reject(new Error(err))
            });
    } catch (err) {
        reject(new Error(err))
    }
});

export default apiCall