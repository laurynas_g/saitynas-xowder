<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <title>{{env('APP_NAME', 'xowder')}}</title>
    <script src="https://unpkg.com/marked@0.3.6"></script>
</head>
<body>
<div id="app">
    <app></app>
</div>
<script src="/js/app.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</body>
</html>