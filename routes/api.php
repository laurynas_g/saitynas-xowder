<?php

use Illuminate\Http\Request;

Route::get('/auth', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::post('register', 'Auth\LoginController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::namespace('Api')->group(function () {

    /*
    / PUBLIC ROUTES
    */
    Route::group(['prefix' => 'communities'], function () {
        Route::get('', 'CommunityController@index');
        Route::group(['prefix' => '{community}'], function () {
            Route::get('', 'CommunityController@get');
            Route::group(['prefix' => 'posts'], function () {
                Route::get('', 'PostController@index');
                Route::group(['prefix' => '{post}'], function () {
                    Route::get('', 'PostController@get');
                    Route::group(['prefix' => 'comments'], function () {
                        Route::get('', 'CommentController@index');
                        Route::group(['prefix' => '{comment}'], function () {
                            Route::get('', 'CommentController@get');
                        });
                    });
                });   
            });
        });
    });

    Route::group(['prefix' => 'community'], function () {
        Route::get('{name}', 'CommunityController@getByName');
    });


    /*
    / PROTECTED ROUTES
    */
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('users', 'UserController@index');
        Route::get('users/me', 'UserController@me');

        Route::post('follow/{user}', 'FollowController@create');
        Route::delete('unfollow/{user}', 'FollowController@delete');

        Route::get('subscriptions', 'SubscriptionController@index');
        Route::post('subscribe', 'SubscriptionController@create');
        Route::delete('unsubscribe/{community}', 'SubscriptionController@delete');
        Route::group(['prefix' => 'communities'], function () {
            Route::post('', 'CommunityController@create');
            Route::group(['prefix' => '{community}'], function () {
                Route::put('', 'CommunityController@update');
                Route::delete('', 'CommunityController@delete');
                Route::group(['prefix' => 'posts'], function () {
                    Route::post('', 'PostController@create');
                    Route::group(['prefix' => '{post}'], function () {
                        Route::put('', 'PostController@update');
                        Route::delete('', 'PostController@delete');

                        Route::post('like', 'PostRatingController@create');
                        Route::delete('dislike', 'PostRatingController@delete');

                        Route::group(['prefix' => 'comments'], function () {
                            Route::post('', 'CommentController@create');
                            Route::group(['prefix' => '{comment}'], function () {
                                Route::put('', 'CommentController@update');
                                Route::delete('', 'CommentController@delete');
                            });
                        });
                    });   
                });
            });
        });
    });
});
