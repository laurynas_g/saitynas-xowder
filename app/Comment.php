<?php

namespace App;

class Comment extends Content
{
    protected $fillable = [
        'user_id',
        'post_id',
        'comment_id',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function ratings()
    {
        return $this->hasMany(CommentRating::class);
    }
    
    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }
}
