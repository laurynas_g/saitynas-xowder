<?php

namespace App\Traits;

use App\Rating;

trait Likeable
{

    public function getScoreAttribute()
    {
        return $this->ratings()->where('rating', Rating::LIKE)->count() - $this->ratings()->where('rating', Rating::DISLIKE)->count();
    }

    public function getLikeCountAttribute()
    {
        return $this->ratings()->where('rating', Rating::LIKE)->count();
    }

    public function getDislikeCountAttribute()
    {
        return $this->ratings()->where('rating', Rating::LIKE)->count();
    }

}