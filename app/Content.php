<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Likeable;

abstract class Content extends Model
{
    use Likeable;

    abstract public function ratings();
}
