<?php

namespace App;

use App\Traits\Likeable;

class Post extends Content
{
    use Likeable;

    protected $fillable = [
        'title',
        'user_id',
        'community_id',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function ratings()
    {
        return $this->hasMany(PostRating::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function isLiked($user_id)
    {
        return (PostRating::ofUser($user_id)->ofPost($this->id)->ofRating(PostRating::LIKE)->count() > 0);
    }

    public function isDisliked($user_id)
    {
        return (PostRating::ofUser($user_id)->ofPost($this->id)->ofRating(PostRating::DISLIKE)->count() > 0);
    }
}
