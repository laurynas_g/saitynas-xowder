<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class Rating extends Model
{
    const LIKE = 'like';
    const DISLIKE = 'dislike';

    public function scopeLikes($query)
    {
        return $query->where('rating', self::LIKE);
    }

    public function scopeDislikes($query)
    {
        return $query->where('rating', self::DISLIKE);
    }
}
