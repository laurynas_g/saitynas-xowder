<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->user->name,
            'community_id' => $this->community_id,
            'score' => $this->getScoreAttribute(),
            'upvoted' => (Auth::user() ? $this->isLiked(Auth::user()->id) : false),
            'downvoted' => (Auth::user() ? $this->isDisliked(Auth::user()->id) : false),
            'comments' => $this->comments->count(),
        ];
    }
}
