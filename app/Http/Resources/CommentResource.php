<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->title,
            'post_id' => $this->title,
            'comment_id' => $this->author,
            'content' => $this->content,
            'created_at' => $this->content,
            'updated_at' => $this->content,
        ];
    }
}
