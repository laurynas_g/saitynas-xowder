<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Follow;
use App\User;

class FollowController extends Controller
{
    public function create(User $user)
    {
        return Follow::firstOrCreate([
            'author_id' => $user->id,
            'user_id' => Auth::user()->id,
        ]);
    }

    public function delete(User $user)
    {
        $follow = Follow::where('author_id', $user->id)->where('user_id', Auth::user()->id)->first();
        $follow->delete();

        return response('', 204);
    }
}
