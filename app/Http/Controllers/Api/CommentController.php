<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Comment;
use App\Community;
use App\Post;
use App\Http\Resources\CommentResource;

class CommentController extends Controller
{
    public function index(Community $community, Post $post)
    {
        return ($community->posts->where('id', $post->id)->first())->comments;
    }

    public function get(Community $community, Post $post, Comment $comment)
    {
        return ($community->posts->where('id', $post->id)->first())->comments->where('id', $comment->id)->first();
    }

    public function create(Community $community, Post $post)
    {
        $request->validate([
            'content' => 'required|max:4000',
        ]);

        return Comment::create([
            'user_id' => Auth::user()->id,
            'post_id' => $post->id,
            'comment_id' => (array_key_exists('reply_to', $request) ? $request['reply_to'] : null),
            'content' => $request['content'],
        ]);
    }

    public function update(Community $community, Post $post, Comment $comment)
    {
        
    }

    public function delete(Community $community, Post $post, Comment $comment)
    {
        $comment = ($community->posts->where('id', $post->id)->first())->comments->where('id', $comment->id)->first();
        $comment->delete();

        return response('', 204);
    }
}
