<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Community;
use App\Http\Resources\CommunityResource;
use Illuminate\Support\Facades\Log;

class CommunityController extends Controller
{
    public function index()
    {
        return CommunityResource::collection(Community::all());
    }

    public function get(Community $community)
    {
        return new CommunityResource($community);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            '*.name' => 'required|max:100|min:3|unique:communities',
        ]);

        $community = Community::create([
            'name' => $request['data']['name'],
            'user_id' => Auth::user()->id,
        ]);

        try {
            Subscription::create([
                'community_id' => $community->id,
                'user_id' => Auth::user()->id,
            ]);
        } catch (\Exception $exception) {
            Log::error('Failed creating default subscription', ['message' => $exception->getMessage()]);
            return $exception;
        }

        return new CommunityResource($community, 201);
    }

    public function update(Request $request, Community $community)
    {
        $request->validate([
            'name' => 'required|max:100|unique:communities',
        ]);

        $community->update([
            'name' => $request['name'],
        ]);

        return response(new CommunityResource($community), 200);
    }

    public function delete(Community $community)
    {
        $community->delete();

        return response('', 204);
    }

    public function getByName(string $name)
    {
        $community = Community::where('name', $name)->first();

        if (!$community) {
            return response(null, 404);
        }

        return new CommunityResource($community);
    }
}
