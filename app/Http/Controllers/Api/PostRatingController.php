<?php

namespace App\Http\Controllers\Api;

use App\Community;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostRating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostRatingController extends Controller
{
    public function create(Request $request, Community $community, Post $post)
    {
        return PostRating::updateOrCreate([
            'post_id' => $post->id,
            'user_id' => Auth::user()->id,
        ], [
            'rating' => $request['rating'],
        ]);
    }

    public function delete(Post $post)
    {
        $rating = PostRating::where('post_id', $post->id)->where('user_id', Auth::user()->id)->first();
        $rating->delete();

        return response('', 204);
    }
}
