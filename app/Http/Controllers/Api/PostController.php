<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Community;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index(Community $community)
    {
        return PostResource::collection($community->posts);
    }

    public function get(Community $community, Post $post)
    {
        $post = $community->posts->where('id', $post->id)->first();

        if (!$post) {
            return response(null, 404);
        }

        return $community->posts->where('id', $post->id)->first();
    }

    public function create(Request $request, Community $community)
    {
        $request->validate([
            '*.title' => 'required|max:255',
            '*.editorData' => 'required',
        ]);

        $data = $request['data'];

        return new PostResource(Post::create([
            'title' => $data['title'],
            'user_id' => Auth::user()->id,
            'community_id' => $community->id,
            'content' => $data['editorData'],
        ]), 201);
    }

    public function update(Request $request, Community $community, Post $post)
    {
        if (Auth::user()->id == $post->user_id) {
            $post = $community->posts->where('id', $post->id)->first();

            $request->validate([
                '*.title' => 'required|max:255',
                '*.editorData' => 'required',
            ]);

            $data = $request['data'];

            $post->update([
                'title' => $data['title'],
                'community_id' => $community->id,
                'content' => $data['editorData'],
            ]);

            return response(new PostResource($post), 200);
        }
        return response(null, 404);
    }

    public function delete(Community $community, Post $post)
    {
        if (Auth::user()->id == $post->user_id) {
            $post = $community->posts->where('id', $post->id)->first();

            $post->delete();

            return response('', 204);
        }
        return response(null, 404);
    }
}
