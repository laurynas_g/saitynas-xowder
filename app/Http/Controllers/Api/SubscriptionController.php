<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Facades\Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Community;
use App\Subscription;

class SubscriptionController extends Controller
{
    public function index()
    {
        return Auth::user()->communities;
    }

    public function create()
    {
        $community = Community::withName(Request::input('data'))->first();
        try {
            Subscription::firstOrCreate([
                'community_id' => $community->id,
                'user_id' => Auth::user()->id,
            ]);
            return $community;
        } catch (\Exception $exception) {
            return [
                'error' => $exception->getMessage()
            ];
        }
    }

    public function delete(Community $community)
    {
        $subscription = Subscription::where('community_id', $community->id)->where('user_id', Auth::user()->id)->first();
        $subscription->delete();

        return response('', 204);
    }
}
