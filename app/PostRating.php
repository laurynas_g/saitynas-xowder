<?php

namespace App;

use App\Rating;

class PostRating extends Rating
{
    protected $fillable = ['post_id', 'user_id', 'rating'];

    public function scopeOfPost($query, $post_id)
    {
        return $query->where('post_id', $post_id);
    }

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    public function scopeOfRating($query, $rating)
    {
        return $query->where('rating', $rating);
    }
}
