<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Community;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Subscription extends Pivot
{
    public $table = "subscriptions";

    protected $fillable = [
        'community_id', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function scopeOfUser($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }

    public function scopeOfCommunity($query, $community_id)
    {
        return $query->where('community_id', $community_id);
    }
}
